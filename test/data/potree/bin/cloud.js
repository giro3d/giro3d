{
	"version": "1.7",
	"octreeDir": "data",
	"boundingBox": {
		"lx": -3158.35525,
		"ly": -3040.36129,
		"lz": -4535.583,
		"ux": 2522.18675,
		"uy": 2640.18071,
		"uz": 1144.959
	},
	"tightBoundingBox": {
		"lx": -3158.35525,
		"ly": -3040.36129,
		"lz": -4535.583,
		"ux": 2522.18675,
		"uy": 2272.03471,
		"uz": 1058.471
	},
	"pointAttributes": [
		"POSITION_CARTESIAN",
		"COLOR_PACKED"
	],
	"spacing": 38.3214912414551,
	"scale": 0.01,
	"hierarchyStepSize": 4
}
