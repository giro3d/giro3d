{
    "version": "1.7",
    "octreeDir": "data",
    "points": 17716478347,
	"projection": "+proj=utm +zone=10 +ellps=GRS80 +datum=NAD83 +units=m +no_defs",
    "boundingBox": {
        "lx": 643431.76,
        "ly": 3889087.89,
        "lz": -2.72,
        "ux": 736910.93,
        "uy": 3982567.06,
        "uz": 93476.45000000004
    },
    "tightBoundingBox": {
        "lx": 643431.76,
        "ly": 3889087.89,
        "lz": -2.72,
        "ux": 736910.93,
        "uy": 3971486.41,
        "uz": 1093.6000000000001
    },
    "pointAttributes": "LAZ",
    "spacing": 647.6427001953125,
    "scale": 0.01,
    "hierarchyStepSize": 5
}
