type TileCoordinate = {
    z: number;
    x: number;
    y: number;
};

export default TileCoordinate;
